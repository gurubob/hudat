# Hudat

Hudat (pronounced "who dat") is a simple express app to list people on a website, specifically acting as a "face book"
so that people can be associated with their image for easy recongition within a community, club, business, etc.

## Description

We're just getting started, so right now when you run the app it will create a sqlite database with three people in it.
From there you can view them and add more. We'll be adding more features as we go.

Routes are defined in `routes/frontend.js` and `routes/admin.js`.

Frontend routes:

- `/people` - list all people

Admin routes:

- `/admin/people/add` - add a person

There is no authentication yet, so anyone can add a person.

## Installation

Quick steps:

1. Clone the rep
2. Run `npm install`
3. Run `npm start`

During development you can take advantage of `nodemon` to automatically restart the server when you make changes by running `npm run dev`.

## Contributing

You're welcome to contribute to this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
