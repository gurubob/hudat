const sqlite3 = require('sqlite3').verbose();

// Create a new SQLite database
const db = new sqlite3.Database('people.db');

// Create a table to store person data
db.serialize(() => {
	db.run('CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, role TEXT, weight INTEGER)');
});

// Create some people if none exist
db.all('SELECT * FROM people ORDER BY weight', (err, rows) => {
	if(!err && rows.length === 0) {
		db.run('INSERT INTO people (name, role, weight) VALUES (?, ?, ?)', ['Person 1', 'CEO', 1]);
		db.run('INSERT INTO people (name, role, weight) VALUES (?, ?, ?)', ['Person 2', 'HR & Marketing', 2]);
		db.run('INSERT INTO people (name, role, weight) VALUES (?, ?, ?)', ['Person 3', 'IT & Security', 3]);
	}
});

module.exports = db;