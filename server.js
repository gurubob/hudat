const express = require('express');
const database = require('./database');
const view = require('./view');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

global.app = app;
app.db = database;
app.view = view;

// Use body-parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('view engine', 'ejs');

// routes
app.use(require('./routes/frontend'));
app.use(require('./routes/admin'));

// Start the server
app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});

