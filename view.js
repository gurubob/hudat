function View() {
	var self = this;
	self.render = (res, viewName, data) =>
		res.render('layout/layout', {viewName: app.get('views')+'/'+viewName, data: data});
}

let view = new View();
module.exports = view;