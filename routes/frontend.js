const express = require('express');
const router = express.Router();

// Get all people from the database
router.get('/people', (req, res) => {
	global.app.db.all('SELECT * FROM people ORDER BY weight', (err, rows) => {
		if (err) {
			console.error(err);
			res.status(500).send('Error retrieving people from database');
		} else {
			global.app.view.render(res, 'people', { people: rows });
		}
	});
});

module.exports = router;