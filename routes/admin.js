const express = require('express');
const router = express.Router();

// Add a new person to the database
router.post('/admin/people', (req, res) => {
	const { name, role, weight } = req.body;

	global.app.db.run('INSERT INTO people (name, role, weight) VALUES (?, ?, ?)', [name, role, weight], function(err) {
		if (err) {
			console.error(err);
			res.status(500).send('Error inserting person into database');
		} else {
			res.redirect('/people');
		}
	});
});

router.get('/admin/people/add', (req, res) => {
	// show the html form to add a person
	global.app.view.render(res, 'admin/person-add');
});

module.exports = router;